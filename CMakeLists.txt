project(serverclient)

cmake_minimum_required(VERSION 3.20.0)

#set(CMAKE_CXX_COMPILER /usr/bin/clang++-12)
set(CMAKE_CXX_COMPILER /usr/bin/g++-10)

#set(CMAKE_CXX_FLAGS "-std=c++11")
set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_CXX_STANDARD 20)

find_package(Threads)
find_package(Boost COMPONENTS system)
find_package(Boost COMPONENTS thread REQUIRED)

#set(CMAKE_CXX_CLANG_TIDY clang-tidy-10 -checks=-*,readability-*)
add_executable(server.exe server.cpp)
add_executable(client.exe client.cpp)

target_link_libraries(server.exe PRIVATE Threads::Threads ${Boost_LIBRARIES} )
target_link_libraries(client.exe PRIVATE Threads::Threads ${Boost_LIBRARIES} )
